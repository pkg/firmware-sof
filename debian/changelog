firmware-sof (2024.09.2-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 12:02:01 +0000

firmware-sof (2024.09.2-1) unstable; urgency=medium

  [ Mark Pearson ]
  * Update to upstream version 2024.09.2
  * Add sof-ipc4-lib directory to package for Lunarlake

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Thu, 05 Dec 2024 18:34:50 +0000

firmware-sof (2024.09.1-1) unstable; urgency=medium

  [ Mark Pearson ]
  * Update to upstream version 2024.09.1

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Tue, 12 Nov 2024 15:28:00 +0000

firmware-sof (2024.06-2) unstable; urgency=medium

  [ Arnaud Rebillout ]
  * Use lintian-overrides to correct use of dpkg-maintscript-helper

  [ Michael Biebl ]
  * Use correct path for dir_to_symlink depending on which version we upgrade
    from (pre and post usrmove).

  [ Mark Pearson ]
  * Update to version 2024.06-2 to include Michael's path fix (via email)

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Mon, 07 Oct 2024 19:16:33 +0000

firmware-sof (2024.06-1) unstable; urgency=medium

  * Improvements to packaging as recommended by Vincent Bernat
  * Update to upstream version 2024-06 

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Mon, 09 Sep 2024 18:55:13 +0000

firmware-sof (2023.12.1-1) unstable; urgency=medium

  * Update to upstream version 2023-12.1

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Tue, 05 Mar 2024 09:52:23 -0500

firmware-sof (2023.12-1) unstable; urgency=medium

  * Update to upstream version 2023-12 

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Thu, 21 Dec 2023 14:48:11 -0500

firmware-sof (2023.09-1) unstable; urgency=medium

  * Update to upstream version 2023-09

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Wed, 04 Oct 2023 15:18:12 -0400

firmware-sof (2.2.6-1) unstable; urgency=medium

  * Update to upstream version 2.2.6

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Thu, 06 Jul 2023 11:21:07 -0400

firmware-sof (2.2.5-1) unstable; urgency=medium

  * Update to upstream version 2.2.5 

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Fri, 12 May 2023 13:06:40 -0400

firmware-sof (2.2.4-1+apertis1) apertis; urgency=medium

  * Bump changelog to trigger license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 27 Jul 2023 14:34:04 +0530

firmware-sof (2.2.4-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 05 May 2023 19:19:46 +0000

firmware-sof (2.2.4-1) unstable; urgency=medium

  [ Cyril Brulebois ]
  * Move source and binary from non-free/kernel to non-free-firmware/kernel
    following the 2022 General Resolution about non-free firmware.
  * Adjust disclaimer in the copyright file as well.

  [ Mark Pearson ]
  * Update to upstream version 2.2.4
  * Update email address

 -- Mark Pearson <mpearson-lenovo@squebb.ca>  Thu, 19 Jan 2023 16:14:28 -0500

firmware-sof (2.2.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + firmware-sof-signed: Drop versioned constraint on dpkg in Pre-Depends.

  [ Mark Pearson ]
  * Update to upstream version 2.2.3 

 -- Mark Pearson <markpearson@lenovo.com>  Fri, 09 Dec 2022 15:55:57 -0500

firmware-sof (2.2.2-1) unstable; urgency=medium

  * Update to upstream version 2.2.2 - Using tarball release.
  * Use dpkg-maintscript-helper to convert sof-tplg from symlink to dir.

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 03 Oct 2022 20:03:19 -0400

firmware-sof (2.1.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.0, no changes needed.

  [ Mark Pearson ]
  * Update to upstream version 2.1.1

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 24 Apr 2022 22:04:04 -0400

firmware-sof (2.0-1) unstable; urgency=medium

  * Update to upstream version 2.0

 -- Mark Pearson <markpearson@lenovo.com>  Fri, 21 Jan 2022 22:35:39 -0500

firmware-sof (1.9-1) unstable; urgency=medium

  * Update to upstream version 1.9

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 04 Oct 2021 22:35:20 -0400

firmware-sof (1.7-1+apertis1) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Thu, 21 Jul 2022 14:06:45 -0300

firmware-sof (1.7-1+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 21 Jul 2022 12:10:00 -0300

firmware-sof (1.7-1) unstable; urgency=medium

  * Update to upstream version 1.7

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 13 Jun 2021 14:05:46 -0400

firmware-sof (1.6.1-2) unstable; urgency=medium

  * Remove postinst file as we don't need to add files to initramfs
  * Add XS-Autobuild configuration to control file

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 18 Jan 2021 13:13:10 -0500

firmware-sof (1.6.1-1) unstable; urgency=medium

  * Update to upstream version 1.6.1

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 04 Jan 2021 21:03:12 -0500

firmware-sof (1.6~rc3-1) unstable; urgency=medium

  * Initial release. (Closes: #960788, #962134)

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 13 Dec 2020 18:02:46 -0500
